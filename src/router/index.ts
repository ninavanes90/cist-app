import { createRouter, createWebHistory } from '@ionic/vue-router';
import { RouteRecordRaw } from 'vue-router';
import Home from '../views/Home.vue';
import Sitemap from  '../views/Sitemap.vue';
import Login from '../views/loginflow/Login.vue';
import ResetPassword from '../views/loginflow/ResetPassword.vue';
import CreateCrisis from '../views/crisis/CreateCrisis.vue';
import CrisisUpdates from '../views/crisis/CrisisUpdates.vue';
import CreateUpdate from '../views/crisis/CreateUpdate.vue';
import EditCrisis from '../views/crisis/EditCrisis.vue';
import Chat from '../views/Chat.vue';
import CrisisBanners from '../views/crisis/CrisisBanners.vue';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/home',
    name: 'Home',
    component: Home, 
  },
  {
    path: '/editcrisis',
    name: 'EditCrisis',
    component: EditCrisis
  },    
  {
    path: '/crisis/:crisis/updates',
    name: 'CrisisUpdates',
    component: CrisisUpdates
  },    
  {
    path: '/crisis/:crisis/createupdate',
    name: 'CreateUpdate',
    component: CreateUpdate
  },  
  {
    path: '/crisis/:crisis/banners',
    name: 'CrisisBanners',
    component: CrisisBanners
  },    
  {
    path: '/login',
    name: 'Login',
    component: Login
  },  
  {
    path: '/reset',
    name: 'ResetPassword',
    component: ResetPassword
  },  
  {
    path: '/createcrisis',
    name: 'CreateCrisis',
    component: CreateCrisis
  },
  {
    path: '/chat',
    name: 'Chat',
    component: Chat
  }, 
  {
    path: '/sitemap',
    name: 'Sitemap',
    component: Sitemap
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
