import { defineComponent } from 'vue';
import { ref, onMounted, watch } from 'vue';
import { Camera, CameraResultType, CameraSource, Photo } from '@capacitor/camera';
import { Filesystem, Directory } from '@capacitor/filesystem';
import { Storage } from '@capacitor/storage';

export const SharedMixin = defineComponent({
    computed: {
        isLoggedIn: function () {
            return localStorage.getItem("loggedIn") == 'true';
        }
    },
    data() {
        return {
            dummydata: [
                { 
                  id: "1",
                  title: "Brand",
                  body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                  updates: [
                    {
                      id: '943',
                      body: 'Update 1'
                    }
                  ]
                  },  
                  {
                    id: "2",
                  title: "Ontploffing",
                  body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.",
                  updates: []
                  },
                  { 
                  id: "3",
                  title: "Brand2",
                  body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                  updates: [
                    {
                      id: '943',
                      body: 'Update 1'
                    }
                  ]
                  },  
                  {
                    id: "4",
                  title: "Ontploffing2",
                  body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.",
                  updates: []
                  },
                  { 
                  id: "5",
                  title: "Brand3",
                  body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                  updates: [
                    {
                      id: '943',
                      body: 'Update 1'
                    }
                  ]
                  },  
                  {
                    id: "6",
                  title: "Ontploffing3",
                  body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.",
                  updates: []
                  },
        
              ],
              dummyLocalStorage: localStorage.getItem('crisises') as string
        }
    },
    methods: {
        getAllCrisises: function () {
            let crisisArray;

            if(this.dummyLocalStorage) {
                crisisArray = JSON.parse(this.dummyLocalStorage);
            } else {
                crisisArray = this.dummydata;
                localStorage.setItem('crisises', JSON.stringify(this.dummydata))
            }

            return crisisArray;
        }
    }
})